import torch,os,time
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
print(torch.cuda.device_count())
print(time.time())

import torch
import os
import time
from PIL import Image
from transformers import TrOCRProcessor, VisionEncoderDecoderModel
import pandas as pd
from itertools import chain
import torch.nn.functional as F

# # Set the environment variable for CUDA
# os.environ["CUDA_VISIBLE_DEVICES"] = "0,1"
# # Check and print the number of available GPUs
# print(torch.cuda.device_count())
# print(time.time())

# Set the device to GPU if available, else CPU
device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using device: {device}")
num_gpus = torch.cuda.device_count()
print(f"Number of GPUs available: {num_gpus}")

# Path to the directory containing images
path = "/home/vishwam/mountpoint/nithish/trocr/goldendata/bw_poa_poi_crop/"
out_csv="/home/vishwam/mountpoint/nithish/trocr/goldendata/golden results/bw_data_chept_best_latest.csv"
k = os.listdir(path)[:]

# Load the processor and model
start_time_processor = time.time()
loaded_preprocessor = TrOCRProcessor.from_pretrained('microsoft/trocr-small-printed')
end_time_processor = time.time()
print("Processor loaded, time taken: ", end_time_processor - start_time_processor)

start_time_model = time.time()
model = VisionEncoderDecoderModel.from_pretrained("/home/vishwam/mountpoint/nithish/models/chept_best_latest/").to(device)
model = torch.nn.DataParallel(model)  # Enable multi-GPU processing
end_time_model = time.time()
print("Model loaded, time taken: ", end_time_model - start_time_model)

# Set special tokens and beam search parameters
model.module.config.pad_token_id = loaded_preprocessor.tokenizer.pad_token_id
model.module.config.vocab_size = model.module.config.decoder.vocab_size
model.module.config.eos_token_id = loaded_preprocessor.tokenizer.sep_token_id
model.module.config.max_length = 128
model.module.config.early_stopping = True
model.module.config.no_repeat_ngram_size = 0
model.module.config.length_penalty = 2.0
model.module.config.num_beams = 4

# Initialize lists to store results
text = []
images_name = []
conf_plus_one_ = []
conf_exp_ = []
confdence_ = []

# Batch processing parameters
batch_size = 8
num_batches = len(k) // batch_size + (1 if len(k) % batch_size != 0 else 0)

start_time_test = time.time()

# Process images in batches
for batch_idx in range(num_batches):
    batch_start = batch_idx * batch_size
    batch_end = min((batch_idx + 1) * batch_size, len(k))
    batch_files = k[batch_start:batch_end]
    
    print(f'Processing batch {batch_idx + 1}/{num_batches}')
    
    # Load and preprocess images in the batch
    images = []
    for file_name in batch_files:
        image = Image.open(os.path.join(path, file_name)).convert("RGB")
        images.append(loaded_preprocessor(image, return_tensors="pt").pixel_values)

    # Concatenate the pixel values to form a batch
    pixel_values = torch.cat(images, dim=0).to(device)
    
    # Generate text and compute scores for the batch
    with torch.no_grad():
        generated_ids = model.module.generate(pixel_values, output_scores=True, return_dict_in_generate=True)
        scores = generated_ids['sequences_scores'].cpu().tolist()
        generated_ids1 = model.module.generate(pixel_values, output_logits=True, return_dict_in_generate=True)
        scores1 = generated_ids1['logits']
    
    # Calculate confidence for the batch
    concat = torch.cat([s.unsqueeze(0) for s in scores1], dim=0)
    confidence = (torch.mean(torch.max(F.softmax(concat, dim=-1), dim=-1).values, dim=1)).cpu().tolist()

    # Ensure the number of confidences matches the batch size
    if len(confidence) != len(batch_files):
        print(f"Warning: Mismatch in confidence length {len(confidence)} and batch size {len(batch_files)}")

    # Extract token IDs and process results for each image in the batch
    for idx, file_name in enumerate(batch_files):
        flat_generated_ids = generated_ids['sequences'][idx].cpu().tolist()
        scores_plus_one = [float(scores[idx]) + 1]
        scores_exp = torch.exp(torch.tensor(scores[idx])).tolist()
        
        generated_text = loaded_preprocessor.tokenizer.decode(flat_generated_ids, skip_special_tokens=True)
        
        # Print and append results
        print(file_name, generated_text, scores_plus_one, scores_exp, confidence[idx])
        
        text.append(generated_text)
        images_name.append(file_name)
        conf_plus_one_.append(str(scores_plus_one).replace('[', '').replace(']', ''))
        conf_exp_.append(str(scores_exp).replace('[', '').replace(']', ''))
        confdence_.append(confidence[idx])

end_time_test = time.time()
print("Inference, time taken: ", end_time_test - start_time_test)

# Save results to a CSV file
df = pd.DataFrame(list(zip(images_name, text, conf_plus_one_, conf_exp_, confdence_)), columns=['file_name', 'text', 'conf_plus_one_', 'conf_exp_', 'confdence_'])
df.to_csv(out_csv, index=False)


# import torch,os,time
# os.environ["CUDA_VISIBLE_DEVICES"] = "0,1"
# print(torch.cuda.device_count())
# print(time.time())

# from PIL import Image
# from transformers import TrOCRProcessor, VisionEncoderDecoderModel
# import os
# import time
# import torch
# import pandas as pd
# from itertools import chain
# import torch.nn.functional as F
# # Check if a GPU is available and set the device accordingly
# # torch.cuda.set_device(-1)
# device = "cuda" if torch.cuda.is_available() else "cpu"
# print(f"Using device: {device}")
# num_gpus = torch.cuda.device_count()
# print(f"Number of GPUs available: {num_gpus}")

# path = "/home/vishwam/mountpoint/nithish/trocr/goldendata/colour_poa_poi_crop/"
# k = os.listdir(path)

# start_time_processor = time.time()
# loaded_preprocessor = TrOCRProcessor.from_pretrained('microsoft/trocr-small-printed')
# end_time_processor = time.time()
# print("Processor loaded, time taken: ", end_time_processor - start_time_processor)

# start_time_model = time.time()
# model = VisionEncoderDecoderModel.from_pretrained("/home/vishwam/mountpoint/nithish/models/chept_best_latest/").to(device)
# end_time_model = time.time()
# print("Model loaded, time taken: ", end_time_model - start_time_model)

# # Set special tokens used for creating the decoder_input_ids from the labels
# model.config.pad_token_id = loaded_preprocessor.tokenizer.pad_token_id
# model.config.vocab_size = model.config.decoder.vocab_size

# # Set beam search parameters
# model.config.eos_token_id = loaded_preprocessor.tokenizer.sep_token_id
# model.config.max_length = 128
# model.config.early_stopping = True
# model.config.no_repeat_ngram_size = 0
# model.config.length_penalty = 2.0
# model.config.num_beams = 4

# start_time_test = time.time()
# text = []
# images_name = []
# conf_plus_one_=[]
# conf_exp_=[]
# confdence_=[]
# c=0
# for i in k:
#     c=c+1
#     print('c : ',c)
#     image = Image.open(os.path.join(path, i)).convert("RGB")
#     pixel_values = loaded_preprocessor(image, return_tensors="pt").pixel_values.to(device)
#     generated_ids = model.generate(pixel_values, output_scores=True, return_dict_in_generate=True)
#     scores = generated_ids['sequences_scores']
#     generated_ids1 = model.generate(pixel_values, output_logits=True, return_dict_in_generate=True)
#     scores1 = generated_ids1['logits']
#     concat = torch.cat(scores1, dim=0)
#     confidence = (torch.mean(torch.max(F.softmax(concat, dim=1), dim=1).values)).item()

#     # Extracting the token IDs from the 'sequences' key in generated_ids
#     generated_ids = generated_ids['sequences']

#     # Flatten the list of token IDs
#     flat_generated_ids = list(chain.from_iterable(generated_ids))

#     # Convert scores to a list of floats
#     scores_plus_one = [float(score) + 1 for score in scores]
#     scores_exp = torch.exp(scores)

#     # Decode the token IDs
#     generated_text = loaded_preprocessor.tokenizer.decode(flat_generated_ids, skip_special_tokens=True)

#     print(i, generated_text, scores_plus_one, scores_exp, confidence)
#     text.append(generated_text)
#     images_name.append(i)
#     conf_plus_one_.append(str(scores_plus_one).replace('[','').replace(']',''))
#     conf_exp_.append(str(scores_exp).replace('tensor([','').replace("], device='cuda:0')",''))
#     confdence_.append(confidence)
    
# end_time_test = time.time()
# print("Inference, time taken: ", end_time_test - start_time_test)

# df = pd.DataFrame(list(zip(images_name, text,conf_plus_one_,conf_exp_,confdence_)), columns=['file_name', 'text','conf_plus_one_','conf_exp_','confdence_'])
# df.to_csv("/home/vishwam/mountpoint/nithish/trocr/goldendata/golden results/colordata_chept_best_latest.csv", index=False)
